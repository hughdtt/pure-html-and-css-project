﻿# Pure HTML/CSS 

The aim of this project is to challenge and push my design skills using purely HTML and CSS.

I want to update and improve on my skills regarding these two technologies.

This project acts as a review and polish to these skills.

*  HTML 5 (Semantic Elements, Attributes, Doctype, etc)
*  CSS Fundamentals (Colours, Fonts, Positioning, Box Model)
*  CSS Grid & Flex Box
*  CSS Custom Properties
*  CSS Transitions

# Acknowledgement
Special thanks to http://www.csszengarden.com/ for inspiring me to embark on this journey.

